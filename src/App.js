//I received help from Leanne Benson for step one (adding the todo button)
//I received help from Drew Radcliff for the clear complete function 
import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    storeInput: "",
  };

  // inputTypedValue = (e) => {
  //   e.preventDefault()
  //   this.setState({
  //     storeInput: e.target.value
  //   })
  // }
  

  addItemToList = (e) => {
    if (e.key === "Enter") {
      const newToDo = {
        userId: 1,
        id: Math.floor(Math.random() * 10000),
        title: e.target.value,
        completed: false
      };
      const newToDos = 
        this.state.todos.slice();
        newToDos.push(newToDo);
        this.setState({
          todos: newToDos
        });
        e.target.value = "";
      }
  }

  toggleComplete = (id) => {
    let newToDos = this.state.todos.map(todo => {
      if(todo.id === id){
        return {
          ...todo, 
          completed: !todo.completed
        }
      }
      return todo
    }) 
    this.setState({
      todos: newToDos
    })
  }

  handleDelete = (todoId) => {
    console.log(todoId)
    const newTodos = this.state.todos.filter(
      todo => todo.id !== todoId
    )
    console.log(newTodos)
    this.setState({todos: newTodos})
  }

  clearComplete = () => {
    const removeComplete = this.state.todos.filter(
      todo => todo.completed === false 
    )
    this.setState({todos: removeComplete})
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>Todos</h1>
          <input 
            className="new-todo" 
            placeholder="What needs to be done?" 
            onKeyDown={this.addItemToList}
            autoFocus 
          />
        </header>
        <TodoList todos={this.state.todos} toggleComplete={this.toggleComplete} handleDelete={this.handleDelete} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearComplete}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed} onChange={this.props.toggleComplete} />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleDelete}/> 
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem key={todo.id}  
              title={todo.title} completed={todo.completed} 
              toggleComplete={e => this.props.toggleComplete(todo.id)} 
              handleDelete={e => this.props.handleDelete(todo.id)}
              />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
